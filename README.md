# What is BuyRanks?
BuyRanks is a small and simple plugin that allows you to buy ranks via commands.
* For the full functionality of the plugin, you need **PermissionsEx** and **Vault**.

# Commands
* Show the help of the plugin (**/rank** or **/rank help**)
* Show the version of the plugin (**/rank version**)
* Buy any available rank (**/rank buy &lt;rankname&gt;**)

* Reload the plugin (**/rank reload**)
* Remove a rank of a player (**/rank remove &lt;playername&gt; &lt;rank&gt;**)

# Config
```
# (C) 2018 BuyRanks Plugin written by JND_3004.
# All rights reserved. License https://creativecommons.org/licenses/by-nc-sa/3.0/
# For more informations, visit https://gitlab.com/JND_3004/buyranks

veteran:
  rankname: Veteran
  pex-rankname: veteran
  price: 150
```
**veteran:** <-- is the ingame command. ___/rank buy veteran___

**rankname:** <-- the correct rankname for messages.

**pex-rankname:** <-- the rankname in your own pex config file.

**price:** <-- the price to bought this rank

# Last versions
All versions of the plugin can be found here on GitLab or on SpigotMC.
* [GitLab](https://gitlab.com/JND_3004/buyranks/tree/master/versions)
* [SpigotMC](https://www.spigotmc.org/resources/buyranks.62418/history)