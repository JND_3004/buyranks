package com.jdittmer.BuyRanks;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.jdittmer.BuyRanks.Commands.*;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

public class Main extends JavaPlugin implements Listener {
	public static FileConfiguration config;
	public static File cfile;

	private static final Logger log = Logger.getLogger("Minecraft");
    public static Economy econ = null;
    private static Permission perms = null;
    private static Chat chat = null;
	
	public void onEnable() {
		/* config.yml LOADING */
		config = getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		cfile = new File(getDataFolder(), "config.yml");
		System.out.println(SOPANSIColors.YELLOW + "[BuyRanks] " + SOPANSIColors.GREEN + "config.yml loaded!" + SOPANSIColors.RESET);
		
		if (!setupEconomy() ) {
			log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		setupPermissions();
        setupChat();
        
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
		
		System.out.println(SOPANSIColors.YELLOW + "[BuyRanks] " + SOPANSIColors.GREEN + "The plugin was activated :)" + SOPANSIColors.RESET);
	}
	
	public void onDisable() {
        System.out.println(SOPANSIColors.YELLOW + "[BuyRanks] " + SOPANSIColors.RED + "The plugin was deactivated :(" + SOPANSIColors.RESET);
    }
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		boolean pluginIsOnline = true;
		
		if(pluginIsOnline == true){
			/* CHECK IF USE THE ACTUALLY PLUGIN VERSION */
			if(p.isOp()){
				CheckVersion.checkVersion(p.getName());
				p.sendMessage("");
			}
		}
	}
	
	public static String plgChatStart(){
		return ChatColor.GOLD + "-------------------- " + ChatColor.BOLD + "[BuyRanks]" + ChatColor.BOLD + " -------------------";
	}
	
	public static String plgChatEnd(){
		return ChatColor.GOLD + "-------------------------------------------------------";
	}
	
	public static String noPermissions(){
		return ChatColor.RED + "You do not have enough rights for this action!";
	}
	
	public static String pluginVersion(){
		return Bukkit.getPluginManager().getPlugin("BuyRanks").getDescription().getVersion();
	}
	
	public static String pluginTag(){
		return ChatColor.GOLD + "[BuyRanks] ";
	}
	
	public static String getRankInfo(String rank, String get){
		if(config.getString(rank) != null){
			return config.getString(rank + "." + get);
		}else{
			return "";
		}
	}
	
	/* VAULT PLUGIN */
	public boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    
    public boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }
    
    public boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("rank")){
			if(args.length == 0){
				if(!(sender instanceof Player)){
					sender.sendMessage("[BuyRanks] Only players can execute this command!");
					return true;
				}else{
					Bukkit.dispatchCommand(sender, "rank help");
					return true;
				}
			}else{
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("help")) {
						CommandHelp.cmd(sender);
	    			}
					
					if(args[0].equalsIgnoreCase("version")) {
						CommandVersion.cmd(sender);
	    			}
					
					if(args[0].equalsIgnoreCase("reload")) {
						CommandReload.cmd(sender);
	    			}
    			}

				if(args.length == 2){
					if(args[0].equalsIgnoreCase("buy")){
						CommandBuyRank.cmd(sender, args[1]);
					}
				}
				
				if(args.length == 3){
					if(args[0].equalsIgnoreCase("remove")){
						CommandRemoveRank.cmd(sender, args[1], args[2]);
					}
				}
			}
		}
		
		return true;
	}
	
	public static Economy getEconomy() {
        return econ;
    }
    
    public static Permission getPermissions() {
        return perms;
    }
    
    public static Chat getChat() {
        return chat;
    }
	
}
