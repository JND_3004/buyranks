package com.jdittmer.BuyRanks.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import com.jdittmer.BuyRanks.Main;

public class CommandReload {
	public static boolean cmd(CommandSender sender){
		if(sender.hasPermission("rank.admin.reload")){
			Main.config = YamlConfiguration.loadConfiguration(Main.cfile);
			sender.sendMessage(Main.pluginTag() + ChatColor.GREEN + "The plugin has been reloaded!");
			return true;
		}else{
			sender.sendMessage(Main.pluginTag() + Main.noPermissions());
			return true;
		}
	}
}
