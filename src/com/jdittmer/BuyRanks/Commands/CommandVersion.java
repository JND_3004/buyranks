package com.jdittmer.BuyRanks.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.jdittmer.BuyRanks.Main;

public class CommandVersion {
	public static boolean cmd(CommandSender sender){
		if(sender.hasPermission("rank.user.version")){
			sender.sendMessage(Main.plgChatStart());
			sender.sendMessage(ChatColor.RED + "== VERSION");
			sender.sendMessage(ChatColor.GOLD + "Version: " + ChatColor.WHITE + Bukkit.getPluginManager().getPlugin("BuyRanks").getDescription().getVersion());
			sender.sendMessage("");
			sender.sendMessage(ChatColor.GOLD + "Written by: " + ChatColor.WHITE + "Justin Dittmer (JND_3004)");
			sender.sendMessage(ChatColor.GOLD + "Website: " + ChatColor.WHITE + Bukkit.getPluginManager().getPlugin("BuyRanks").getDescription().getWebsite());
			sender.sendMessage(ChatColor.GOLD + "Spigot: " + ChatColor.WHITE + "https://www.spigotmc.org/members/jnd_3004.282948/");
			sender.sendMessage(ChatColor.GOLD + "GitLab: " + ChatColor.WHITE + "https://gitlab.com/JND_3004");
			sender.sendMessage("");
			sender.sendMessage(ChatColor.GOLD + "Found a bug? Send a mail: " + ChatColor.WHITE + "incoming+JND_3004/buyranks@gitlab.com");
			sender.sendMessage(Main.plgChatEnd());
			
			return true;
		}else{
			sender.sendMessage(Main.pluginTag() + Main.noPermissions());
			return false;
		}
	}
}
