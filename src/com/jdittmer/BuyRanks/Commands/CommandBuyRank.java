package com.jdittmer.BuyRanks.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.jdittmer.BuyRanks.Main;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class CommandBuyRank {
	@SuppressWarnings("deprecation")
	public static boolean cmd(CommandSender sender, String rank){
		if(sender.hasPermission("rank.user.buy")){
			if(Main.config.getString(rank) != null){
				Player p = (Player) sender;
				PermissionUser pexUser = PermissionsEx.getUser(p);
				if(!pexUser.inGroup(rank)){
					if(Main.econ.getBalance(p.getName()) >= Main.config.getDouble(rank+".price")){
						Main.econ.withdrawPlayer(p.getName(), Main.config.getDouble(rank+".price"));
						pexUser.addGroup(rank);
						p.sendMessage(Main.pluginTag() + ChatColor.GREEN + "You bought the rank " + ChatColor.UNDERLINE + Main.getRankInfo(rank, "rankname") + ChatColor.RESET + ChatColor.GREEN + " :)");
						p.sendMessage(String.format(Main.pluginTag() + ChatColor.RESET + "New Balance: %s", Main.econ.format(Main.econ.getBalance(p.getName()))));
					}else{
						p.sendMessage(Main.pluginTag() + ChatColor.RED + "You do not have enough money to buy this rank!");
					}
				}else{
					p.sendMessage(Main.pluginTag() + ChatColor.RED + "You already have the rank " + ChatColor.UNDERLINE + Main.getRankInfo(rank, "rankname") + ChatColor.RESET + ChatColor.RED + "!");
				}
				
				return true;
			}else{
				sender.sendMessage(Main.pluginTag() + ChatColor.RED + "The rank " + ChatColor.UNDERLINE + rank + ChatColor.RESET + ChatColor.RED + " does not exist!");
				return true;
			}
		}
		return true;
	}
}
