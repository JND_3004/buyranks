package com.jdittmer.BuyRanks.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.jdittmer.BuyRanks.Main;

public class CommandHelp {
	public static boolean cmd(CommandSender sender){
		if(sender.hasPermission("rank.user.help")){
			sender.sendMessage(Main.plgChatStart());
			sender.sendMessage(ChatColor.RED + "== PLAYER COMMANDS");
			if(sender.hasPermission("rank.user.help")){
				sender.sendMessage(ChatColor.GOLD + "/rank help" + ChatColor.RESET + ": Show the help of the plugin");
				sender.sendMessage(ChatColor.GOLD + "/rank version" + ChatColor.RESET + ": Show the version of the plugin");
				sender.sendMessage("");
			}
			if(sender.hasPermission("rank.user.buy")){
				sender.sendMessage(ChatColor.GOLD + "/rank buy <rankname>" + ChatColor.RESET + ": Buy any available rank");
			}
			
			if(sender.hasPermission("rank.admin.reload")){
				sender.sendMessage("");
				sender.sendMessage(ChatColor.RED + "== ADMIN COMMANDS");
				sender.sendMessage(ChatColor.GOLD + "/rank reload" + ChatColor.RESET + ": Reload the plugin");
				sender.sendMessage(ChatColor.GOLD + "/rank remove <playername> <rank>" + ChatColor.RESET + ": Remove a rank of a player");
			}
			sender.sendMessage(Main.plgChatEnd());
			
			return true;
		}else{
			sender.sendMessage(Main.pluginTag() + Main.noPermissions());
			return true;
		}
	}
}
