package com.jdittmer.BuyRanks.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.jdittmer.BuyRanks.Main;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class CommandRemoveRank {
	public static boolean cmd(CommandSender sender, String player, String rank){
		if(sender.hasPermission("rank.admin.remove.userrank")){
			if(Bukkit.getServer().getPlayer(player) != null){
				Player fp = (Player) sender;
				Player tp = Bukkit.getPlayer(player);
				PermissionUser pexUser = PermissionsEx.getUser(player);
				if(pexUser.inGroup(rank)){
					pexUser.removeGroup(rank);
					fp.sendMessage(Main.pluginTag() + ChatColor.GREEN + "The rank " + ChatColor.UNDERLINE + Main.getRankInfo(rank, "rankname") + ChatColor.RESET + ChatColor.GREEN + " was removed from the player " + ChatColor.UNDERLINE + player + ChatColor.RESET + ChatColor.GREEN + "!");
					tp.sendMessage(Main.pluginTag() + ChatColor.RED + "You were deprived of the rank " + ChatColor.UNDERLINE + Main.getRankInfo(rank, "rankname") + ChatColor.RESET + ChatColor.RED + "!");
				}else{
					fp.sendMessage(Main.pluginTag() + ChatColor.RED + "The player " + ChatColor.UNDERLINE + player + ChatColor.RESET + ChatColor.RED + " does not have the rank " + ChatColor.UNDERLINE + Main.getRankInfo(rank, "rankname") + ChatColor.RESET + ChatColor.RED + " at the moment!");
				}
				
				return true;
			}else{
				sender.sendMessage(Main.pluginTag() + ChatColor.RED + "The player " + ChatColor.UNDERLINE + player + ChatColor.RESET + ChatColor.RED + " is offline right now. His rank could not be removed!");
				return true;
			}
		}
		return true;
	}
}
